/*
 * Name: aslrdemo.c
 * Desc: Simple program that prints its stack details 64-bit Linux OS on x86_64 Intel systems.
 * Author: Reuben Johnston, reub@jhu.edu
 * Course: JHU-ISI, Software Vulnerability Analysis, EN.650.660
 * Notes:
 *   * Turn ASLR on in the OS via $ echo 1 | sudo tee /proc/sys/kernel/randomize_va_space
 *   * Turn ASLR off in the OS via $ echo 0 | sudo tee /proc/sys/kernel/randomize_va_space
 *   * Turn pie off: -fPIE -no-pie
 *   * Turn pie on: -fPIE -pie
 * Usage:
 *   $ ./aslrdemo
 * Copyright:
 *   Reuben Johnston, www.reubenjohnston.com
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	char *pheapbuffer;

	puts("!!!Hello EN.650.660 students!!!"); /* prints !!!Hello World!!! */

	register long long int rsp asm ("rsp");

	unsigned long long int addr;

	printf("program's stack rsp is %08llx\n", rsp);

	pheapbuffer=malloc(128);
	addr=pheapbuffer;
	printf("program's heap address for heapbuffer is 0x%016llx\n",addr);

	addr=&main;
	printf("program's main() address is 0x%016llx\n",addr);

	addr=&printf;
	printf("glibc's printf() address is 0x%016llx\n",addr);

	addr=&system;
	printf("vdso system() address ix 0x%016llx\n",addr);

	free(pheapbuffer);

	return EXIT_SUCCESS;
}
